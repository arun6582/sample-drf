from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=256)
    location = models.TextField()


class Sensor(models.Model):
    Company = models.ForeignKey('Company', on_delete=models.CASCADE)
    sensor_id = models.UUIDField()
    active = models.BooleanField(default=True)
