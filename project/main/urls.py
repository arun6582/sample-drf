from django.urls import include, path
from main.views import CompanyViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'company', CompanyViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
